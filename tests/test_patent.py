import numpy as np
import pytest

# Replace 'your_module' with the name of your Python file
from firemap.patent import Camera, Map, Plane, Trajectory


def test_camera_initialization():
    camera_direction = np.array([1., 0., 0.])
    camera = Camera(camera_direction=camera_direction)
    assert np.array_equal(camera.get_camera_direction(
    ), camera_direction), "Camera direction not initialized correctly."
    assert len(camera.get_sensors_array()
               ) == 64, "Sensors array shape is incorrect."
    assert len(camera.get_sensors_array()[
               0]) == 3, "Sensors array shape is incorrect."
    assert np.allclose(np.linalg.norm(camera.get_sensors_array(
    ), axis=1), 1), "Sensors directions are not normalized."


def test_plane_update_coordinate_and_time_and_temperature_points():
    plane = Plane(base_coordinate=np.array([0., 0., 100.]), initial_time=0., camera_direction=np.array(
        [0., 0., -1]), speed=2., expected_time=1000., loops_distance=5., area=[-250, 250, -250, 250])

    initial_time = plane.time
    initial_coordinate = plane.coordinate.copy()
    plane.update_coordinate_and_time(100)
    assert plane.time == 100, "Time was not updated correctly."
    assert not np.array_equal(
        plane.coordinate, initial_coordinate), "Coordinate was not updated correctly."

    # Testing temperature points addition
    initial_points_count = len(plane.map.points)
    plane.add_new_temperature_points()
    assert len(
        plane.map.points) > initial_points_count, "New temperature points were not added."


def test_main_code():
    flight_time = 15 * 60
    area = [-400., 400, -400, 400]

    plane = Plane(speed=6., initial_roll=0.01 * np.pi,
                  expected_time=flight_time, area=area, loops_distance=10)

    print(plane)
    i = 0
    for t in np.linspace(0, flight_time * 0.8, 200):
        if i % 4:
            plane.add_new_temperature_points()
            plane.add_new_flight_point()
        i += 1
        plane.update_coordinate_and_time(t)

    print(plane)
