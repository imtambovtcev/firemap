import math
from typing import Union


class Vector:
    def __init__(self, data: list):
        """Инициализирует объект Vector с заданным списком чисел.

        Args:
            data (list): Список чисел.

        Raises:
            ValueError: Если элементы списка не являются целыми числами или числами с плавающей точкой.
        """
        if not all(isinstance(item, (int, float)) for item in data):
            raise ValueError(
                "Все элементы списка должны быть целыми числами или числами с плавающей точкой.")
        self.data = data

    def __repr__(self) -> str:
        """Возвращает строковое представление объекта Vector."""
        return f"Vector({self.data})"

    def __len__(self) -> int:
        """Возвращает длину объекта Vector."""
        return len(self.data)

    def __iter__(self):
        """Возвращает итератор для объекта Vector."""
        return iter(self.data)

    def __getitem__(self, key) -> 'Vector':
        """Возвращает элемент или срез объекта Vector."""
        return self.data[key]

    def __setitem__(self, key, value) -> None:
        """Устанавливает значение элемента или среза объекта Vector."""
        self.data[key] = value

    def __eq__(self, other: Union[list, 'Vector']) -> bool:
        """Проверяет равенство двух объектов Vector.

        Args:
            other (Union[list, 'Vector']): Другой объект Vector или список.

        Returns:
            bool: True, если объекты равны, иначе False.
        """
        if isinstance(other, Vector):
            return self.data == other.data
        if isinstance(other, list):
            return self.data == other

    def __add__(self, other: Union['Vector', int, float]) -> 'Vector':
        """Выполняет поэлементное сложение с другим объектом Vector или скаляром.

        Args:
            other (Union['Vector', int, float]): Другой объект Vector или скаляр.

        Returns:
            'Vector': Результат сложения.

        Raises:
            ValueError: Если длины векторов не совпадают.
            TypeError: Если тип операнда не поддерживается.
        """
        if isinstance(other, Vector):
            if len(self.data) != len(other.data):
                raise ValueError("Векторы должны иметь одинаковую длину.")
            return Vector([x + y for x, y in zip(self.data, other.data)])
        elif isinstance(other, (int, float)):
            return Vector([x + other for x in self.data])
        else:
            raise TypeError("Неподдерживаемый тип операнда для +.")

    def __sub__(self, other: Union['Vector', int, float]) -> 'Vector':
        """Выполняет поэлементное вычитание с другим объектом Vector или скаляром.

        Args:
            other (Union['Vector', int, float]): Другой объект Vector или скаляр.

        Returns:
            'Vector': Результат вычитания.

        Raises:
            ValueError: Если длины векторов не совпадают.
            TypeError: Если тип операнда не поддерживается.
        """
        if isinstance(other, Vector):
            if len(self.data) != len(other.data):
                raise ValueError("Векторы должны иметь одинаковую длину.")
            return Vector([x - y for x, y in zip(self.data, other.data)])
        elif isinstance(other, (int, float)):
            return Vector([x - other for x in self.data])
        else:
            raise TypeError("Неподдерживаемый тип операнда для -.")

    def __mul__(self, value: float):
        """Выполняет умножение всех элементов вектора на заданное значение.

        Args:
            value (float): Значение для умножения.

        Returns:
            'Vector': Результат умножения.
        """
        return Vector([x * value for x in self.data])

    def __truediv__(self, value: float):
        """Выполняет деление всех элементов вектора на заданное значение.

        Args:
            value (float): Значение для деления.

        Returns:
            'Vector': Результат деления.
        """
        return Vector([x / value for x in self.data])


class Matrix:
    def __init__(self, data: list):
        """Инициализирует объект Matrix с данными матрицы, представленными в виде списка списков."""
        if not all(isinstance(row, list) for row in data) or not all(len(row) == len(data[0]) for row in data):
            raise ValueError(
                "Все строки данных должны иметь одинаковую длину.")
        self.data = data

    def __repr__(self) -> str:
        """Возвращает строковое представление объекта Matrix."""
        return f"Matrix({self.data})"

    def __len__(self) -> int:
        """Возвращает количество строк в матрице."""
        return len(self.data)

    def __getitem__(self, key) -> 'Vector':
        """Возвращает строку матрицы."""
        return Vector(self.data[key])

    def __setitem__(self, key, value) -> None:
        """Устанавливает строку матрицы."""
        self.data[key] = value

    def __eq__(self, other: Union[list, 'Matrix']) -> bool:
        """Проверяет равенство двух объектов Matrix."""
        if isinstance(other, Matrix):
            return self.data == other.data
        if isinstance(other, list):
            return self.data == other

    @ property
    def transpose(self) -> 'Matrix':
        """Возвращает транспонированную матрицу."""
        return Matrix([list(col) for col in zip(*self.data)])

    def __add__(self, other: Union['Matrix', int, float]) -> 'Matrix':
        """Выполняет поэлементное сложение с другой матрицей или скаляром."""
        if isinstance(other, Matrix):
            if len(self.data) != len(other.data) or len(self.data[0]) != len(other.data[0]):
                raise ValueError("Матрицы должны иметь одинаковые размеры.")
            return Matrix([[x + y for x, y in zip(row1, row2)] for row1, row2 in zip(self.data, other.data)])
        elif isinstance(other, (int, float)):
            return Matrix([[x + other for x in row] for row in self.data])
        else:
            raise TypeError("Неподдерживаемый тип операнда для +.")

    def __sub__(self, other: Union['Matrix', int, float]) -> 'Matrix':
        """Выполняет поэлементное вычитание с другой матрицей или скаляром."""
        if isinstance(other, Matrix):
            if len(self.data) != len(other.data) or len(self.data[0]) != len(other.data[0]):
                raise ValueError("Матрицы должны иметь одинаковые размеры.")
            return Matrix([[x - y for x, y in zip(row1, row2)] for row1, row2 in zip(self.data, other.data)])
        elif isinstance(other, (int, float)):
            return Matrix([[x - other for x in row] for row in self.data])
        else:
            raise TypeError("Неподдерживаемый тип операнда для -.")

    def __mul__(self, other: Union['Matrix', 'Vector', int, float]) -> Union['Matrix', 'Vector']:
        """Выполняет умножение матрицы на другую матрицу, вектор или скаляр."""
        if isinstance(other, Matrix):
            if len(self.data[0]) != len(other.data):
                raise ValueError(
                    "Количество столбцов в первой матрице должно совпадать с количеством строк во второй матрице.")
            result = [[sum(a * b for a, b in zip(row1, col2))
                       for col2 in zip(*other.data)] for row1 in self.data]
            return Matrix(result)
        elif isinstance(other, Vector):
            if len(self.data[0]) != len(other.data):
                raise ValueError(
                    "Количество столбцов в матрице должно совпадать с длиной вектора.")
            result = [sum(a * b for a, b in zip(row, other.data))
                      for row in self.data]
            return Vector(result)
        elif isinstance(other, (int, float)):
            return Matrix([[x * other for x in row] for row in self.data])
        else:
            raise TypeError("Неподдерживаемый тип операнда для *.")

    @ staticmethod
    def rotation_x(angle: float) -> 'Matrix':
        """Генерирует матрицу поворота вокруг оси x в трехмерном пространстве."""
        return Matrix([
            [1, 0, 0],
            [0, math.cos(angle), -math.sin(angle)],
            [0, math.sin(angle), math.cos(angle)]
        ])

    @ staticmethod
    def rotation_y(angle: float) -> 'Matrix':
        """Генерирует матрицу поворота вокруг оси y в трехмерном пространстве."""
        return Matrix([
            [math.cos(angle), 0, math.sin(angle)],
            [0, 1, 0],
            [-math.sin(angle), 0, math.cos(angle)]
        ])

    @ staticmethod
    def rotation_z(angle: float) -> 'Matrix':
        """Генерирует матрицу поворота вокруг оси z в трехмерном пространстве."""
        return Matrix([
            [math.cos(angle), -math.sin(angle), 0],
            [math.sin(angle), math.cos(angle), 0],
            [0, 0, 1]
        ])

    @ classmethod
    def from_euler(cls, sequence: str, angles: list) -> 'Matrix':
        """Создает матрицу поворота в трехмерном пространстве на основе углов Эйлера."""
        rotation_matrix = cls.identity(3)  # Начинаем с единичной матрицы

        for axis, angle in zip(sequence, angles):
            if axis == 'x':
                rotation_matrix *= cls([
                    [1, 0, 0],
                    [0, math.cos(angle), -math.sin(angle)],
                    [0, math.sin(angle), math.cos(angle)]
                ])
            elif axis == 'y':
                rotation_matrix *= cls([
                    [math.cos(angle), 0, math.sin(angle)],
                    [0, 1, 0],
                    [-math.sin(angle), 0, math.cos(angle)]
                ])
            elif axis == 'z':
                rotation_matrix *= cls([
                    [math.cos(angle), -math.sin(angle), 0],
                    [math.sin(angle), math.cos(angle), 0],
                    [0, 0, 1]
                ])
            else:
                raise NotImplementedError(
                    f"Поворот вокруг оси {axis} не реализован.")

        return rotation_matrix

    @ classmethod
    def from_matrix(cls, input_matrix) -> 'Matrix':
        """Создает объект Matrix из двумерного списка размером 3x3 или массива NumPy."""
        if isinstance(input_matrix, Matrix):
            if len(input_matrix.data) == 3 and all(len(row) == 3 for row in input_matrix.data):
                return input_matrix
            else:
                raise ValueError("Входная матрица должна иметь размер 3x3.")

        if isinstance(input_matrix, list) or isinstance(input_matrix):
            if len(input_matrix) == 3 and all(len(row) == 3 for row in input_matrix):
                return cls(input_matrix)
            else:
                raise ValueError(
                    "Входной список/массив должен иметь размер 3x3.")

        raise TypeError(
            "Входные данные должны быть двумерным списком размером 3x3 или массивом NumPy.")

    @ staticmethod
    def identity(size: int) -> 'Matrix':
        """Создает единичную матрицу заданного размера."""
        return Matrix([[1 if i == j else 0 for j in range(size)] for i in range(size)])

    def apply(self, vectors):
        """Применяет матрицу поворота к объекту Vector или списку объектов Vector."""
        single_vector = False
        if isinstance(vectors, Vector):
            # Преобразование одного объекта Vector в список из одного объекта Vector
            vectors = [vectors]
            single_vector = True

        rotated_vectors = []
        for vector in vectors:
            if not isinstance(vector, Vector):
                raise TypeError(
                    "Входные данные должны быть объектом Vector или списком объектов Vector.")

            # Выполняем умножение матрицы на вектор
            rotated_vector_data = []
            for row in self.data:
                # Умножаем каждую строку матрицы на вектор
                sum_result = sum(
                    row_element * vector_element for row_element, vector_element in zip(row, vector))
                rotated_vector_data.append(sum_result)

            # Преобразуем результат в объект Vector
            rotated_vector = Vector(rotated_vector_data)
            rotated_vectors.append(rotated_vector)

        # Если входные данные были одним объектом Vector, возвращаем один объект Vector, а не список
        return rotated_vectors[0] if single_vector else rotated_vectors

    @classmethod
    def diag(cls, elements: list) -> 'Matrix':
        """Создает диагональную матрицу с заданными элементами на диагонали."""
        size = len(elements)
        # Инициализируем квадратную матрицу нулями
        data = [[0 for _ in range(size)] for _ in range(size)]
        # Устанавливаем элементы на диагонали
        for i in range(size):
            data[i][i] = elements[i]
        return cls(data)


def linspace(start: float, stop: float, num: int = 50) -> 'Vector':
    """
    Генерирует массив линейно распределенных значений между заданным начальным и конечным значениями.

    Args:
    - start (float): Начальное значение последовательности.
    - stop (float): Конечное значение последовательности.
    - num (int): Количество сэмплов для генерации. По умолчанию 50.

    Returns:
    - MyArray: Экземпляр MyArray, содержащий линейно распределенные значения.
    """
    if num < 2:
        raise ValueError("num должно быть не меньше 2.")

    step = (stop - start) / (num - 1)
    # Изменено для создания одномерного списка
    return Vector([start + step * i for i in range(num)])


def meshgrid(x: 'Vector', y: 'Vector') -> tuple['Matrix', 'Matrix']:
    """Генерирует две 2D матрицы, представляющие координаты X и Y сетки.

    Args:
    x (Vector): Вектор X-координат.
    y (Vector): Вектор Y-координат.

    Returns:
    tuple['Matrix', 'Matrix']: Кортеж из двух матриц grid_x и grid_y, представляющих X и Y координаты сетки.

    Exceptions:
    TypeError: Если x или y не являются экземплярами класса Vector.
    ValueError: Если x или y векторы пусты.
    """
    if not isinstance(x, Vector) or not isinstance(y, Vector):
        raise TypeError("x и y должны быть экземплярами класса Vector.")
    if len(x.data) == 0 or len(y.data) == 0:
        raise ValueError("x и y векторы не должны быть пустыми.")

    # Создание grid_x путем повторения вектора x для каждого элемента вектора y
    grid_x = Matrix([x.data for _ in range(len(y.data))])

    # Создание grid_y путем повторения каждого элемента вектора y для формирования строк,
    # совпадающих по длине с вектором x, для каждого элемента вектора x
    grid_y = Matrix([[y_elem for _ in range(len(x.data))]
                     for y_elem in y.data])

    return grid_x, grid_y


def expand_dims(arr: 'Vector', axis: int) -> 'Matrix':
    """Expand the dimensions of a Vector instance by adding a new axis."""
    if not isinstance(arr, Vector):
        raise TypeError("arr must be an instance of Vector.")
    expanded = []
    if axis == 0:
        for val in arr.data:
            expanded.append([val])
    elif axis == 1:
        expanded.append(arr.data)
    else:
        raise ValueError("axis must be 0 or 1.")
    return Matrix(expanded)


def concatenate_to_list_of_vectors(vectors_list: list['Vector']) -> 'Matrix':
    """Создает матрицу, объединяя несколько экземпляров Vector.

    Args:
    vectors_list (list['Vector']): Список экземпляров Vector, которые нужно объединить.

    Returns:
    'Matrix': Матрица, состоящая из объединенных векторов.

    Exceptions:
    ValueError: Если элементы vectors_list не являются экземплярами Vector или списками.

    """
    vectors_list = [vectors if isinstance(
        vectors, list) else vectors.tolist() for vectors in vectors_list]
    list_of_vectors = []

    for vectors in vectors_list:
        for vector in vectors:
            if isinstance(vector, Vector):
                list_of_vectors.append(vector)
            elif isinstance(vector, list):
                list_of_vectors.append(Vector(vector))
            else:
                raise ValueError(
                    "All elements of vectors_list must be of type Vector or list.")

    return list_of_vectors


def concatenate(vectors: list['Vector']) -> 'Matrix':
    """Создает матрицу, объединяя несколько экземпляров класса Vector.

    Args:
    vectors (list['Vector']): Список экземпляров класса Vector.

    Returns:
    'Matrix': Матрица, созданная путем объединения векторов.

    Exceptions:
    TypeError: Если vectors не является списком экземпляров класса Vector.
    ValueError: Если все векторы имеют разную длину.

    Examples:
    >>> v1 = Vector([1, 2, 3])
    >>> v2 = Vector([4, 5, 6])
    >>> concatenate([v1, v2])
    Matrix([[1, 2, 3],
            [4, 5, 6]])
    """
    if not all(isinstance(vector, Vector) for vector in vectors):
        raise TypeError(
            "vectors должен быть списком экземпляров класса Vector.")
    if not all(len(vector) == len(vectors[0]) for vector in vectors):
        raise ValueError("Все векторы должны иметь одинаковую длину.")
    data = [vector.data for vector in vectors]
    return Matrix(data)


def norm(self) -> float:
    """Вычисляет норму вектора."""
    return sum(x**2 for x in self.data)**0.5


def newton_raphson_method(f, df, initial_guess, max_iter=100, tol=1e-6):
    """Метод Ньютона-Рафсона для нахождения корня уравнения f(x) = 0.

    Args:
    f (callable): Функция, корень которой нужно найти.
    df (callable): Производная функции f.
    initial_guess (float): Начальное приближение для корня.
    max_iter (int): Максимальное количество итераций. По умолчанию 100.
    tol (float): Допустимая погрешность. По умолчанию 1e-6.

    Returns:
    float: Найденный корень.

    Examples:
    >>> f = lambda x: x**2 - 4
    >>> df = lambda x: 2*x
    >>> newton_raphson_method(f, df, 3)
    2.0
    """

    theta = initial_guess
    for _ in range(max_iter):
        theta_next = theta - f(theta) / df(theta)
        if abs(theta_next - theta) < tol:
            return theta_next
        theta = theta_next
    return theta
