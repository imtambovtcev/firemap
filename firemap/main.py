#!/usr/bin/env python

import os
import time
from copy import copy

import matplotlib.colorbar
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.colors import Normalize
from matplotlib.patches import Polygon
from scipy.interpolate import griddata, interp1d
from scipy.optimize import fsolve
from scipy.spatial.transform import Rotation as R

setup = 'test'
if setup == 'test':
    pass
elif setup == 'raspberry':
    import adafruit_amg88xx
    import board
    import busio


class Camera:
    # на данный момент предполагается, что камера смотрит
    def __init__(self, camera_direction=np.array([0., 0., -1])):
        # по оси самолета, хотя легко добавить еще одну матрицу поворота,
        # учитывающую и ассиметричное расположение камеры, или же все исправлять с помощью калибровки
        if setup == 'test':
            pass
        elif setup == 'raspberry':
            self.i2c_bus = busio.I2C(board.SCL, board.SDA)
            self.sensor = adafruit_amg88xx.AMG88XX(self.i2c_bus, addr=0x69)
        time.sleep(.1)
        # вектор направления камеры в квадрокоптере, по умолчанию вниз
        self.camera_direction = camera_direction
        x = np.linspace(-0.5, 0.5, 8)
        y = np.linspace(-0.5, 0.5, 8)
        X, Y = np.meshgrid(x, y)
        print(X.shape, Y.shape)
        calibration = np.concatenate(
            [np.expand_dims(X, -1), np.expand_dims(Y, -1), np.ones((8, 8, 1))], axis=-1)
        print(calibration.shape)
        # calibration = np.array([[[-0.1, 0.1, 1], [0, 0.1, 1], [0.1, 0.1, 1]],
        #                        [[-0.1, 0, 1], [0, 0, 1], [0.1, 0, 1]],
        #                        [[-0.1, -0.1, 1], [0, -0.1, 1], [0.1, -0.1, 1]]],dtype=float)
        # сенсор отвечает за точку [x,y,z], если на расстоянии z
        # его зона ответственности находится на удаелении (x,y) от луча

        # print(f'{calibration = }')

        calibration = calibration[:, :, :2] / \
            np.linalg.norm(calibration, axis=2)[..., np.newaxis]
        # нормировка, если она необходима

        calibration = calibration.reshape(
            (-1, 2))  # переход от матрицы к вектору
        self.sensors_array = np.array(
            [R.from_euler('yx', xy).apply(self.camera_direction) for xy in list(np.arcsin(calibration))])
        # print(f'{self.sensors_array = }')

    def get_camera_direction(self):
        return self.camera_direction

    def get_sensors_array(self):
        return self.sensors_array

    def get_temperature_array(self, points, test=False):
        if setup == 'test':
            t_array = (np.exp(-0.002 * ((points[:, 0] - 100) ** 2
                                        + (points[:, 1] - 120) ** 2)) * 50 + 17).reshape(-1, 1) + .3 * np.random.rand()
            return t_array
        elif setup == 'raspberry':
            self.sensor = adafruit_amg88xx.AMG88XX(self.i2c_bus, addr=0x69)
            # Waiting for sensor initialization
            time.sleep(.1)
            # print(self.sensor.pixels)
            # print(np.array(self.sensor.pixels).shape)
            return np.array(self.sensor.pixels).reshape(-1, 1)
        # print(f'{points.shape = }')


class Map:
    def __init__(self, area=[-250, 250, -250, 250]):
        self.points = np.empty((0, 2))
        self.temps = np.empty((0, 1))
        self.times = np.empty((0, 1))
        self.flight_history = np.empty((0, 3))
        self.area = area

    def make_regular_grid(self):
        xn = yn = 100
        x = np.linspace(self.area[0], self.area[1], xn)
        y = np.linspace(self.area[2], self.area[3], yn)
        grid = np.array(np.meshgrid(x, y, indexing='ij'))
        grid = np.moveaxis(grid, 0, -1).reshape(-1, 2)
        # print(f'{grid.shape = }')
        result = griddata(self.points, self.temps, grid)
        # print(f'{result.shape = }')
        return x, y, result.reshape(xn, yn)

    def show_regular_grid(self, fig=None, ax=None):
        if fig is None:
            fig, ax = plt.subplots()
        x, y, result = self.make_regular_grid()
        X, Y = np.meshgrid(x, y, indexing='ij')
        # print(f'{X.shape = } {Y.shape = } {result.shape = } {np.any(np.isnan(result)) = }')
        im = ax.pcolormesh(X, Y, result, shading='auto', alpha=0.9)
        fig.colorbar(im, ax=ax)
        return fig, ax

    def add_points(self, new_points, new_temps, new_times):
        # print(f'{self.points.shape = } {new_points.shape = }')
        # print(f'{self.temps.shape = } {new_temps.shape = }')
        self.points = np.concatenate((self.points, new_points), axis=0)
        self.temps = np.concatenate((self.temps, new_temps), axis=0)
        self.times = np.concatenate((self.times, new_times), axis=0)

    def add_flight_point(self, new_flight_point):
        self.flight_history = np.concatenate(
            (self.flight_history, new_flight_point.reshape(1, 3)), axis=0)

    def show_data_points(self, fig=None, ax=None, **plt_kwargs):
        if fig is None:
            fig, ax = plt.subplots()
        im = ax.scatter(self.points[:, 0], self.points[:, 1],
                        c=self.temps[:, 0], cmap='gnuplot2', vmin=0, vmax=50)
        fig.colorbar(im, ax=ax)
        ax.set_aspect('equal')
        ax.grid()
        return fig, ax

    def show_path(self, fig=None, ax=None, **plt_kwargs):
        if fig is None:
            fig, ax = plt.subplots()
        ax.plot(self.flight_history[:, 0], self.flight_history[:, 1], 'w')
        ax.set_aspect('equal')
        ax.grid()
        return fig, ax

    def show_background(self, fig=None, ax=None, **plt_kwargs):
        if fig is None:
            fig, ax = plt.subplots()
        current_dir = os.path.dirname(os.path.abspath(__file__))
        img_path = os.path.join(current_dir, 'background.jpg')
        img = plt.imread(img_path)
        ax.imshow(img, extent=self.area, alpha=0.7)
        return fig, ax


class Trajectory:  # Архимедова спираль
    def __init__(self, base_coordinate=np.array([0., 0., 100.]), min_radius=0., loops_distance=5., time=2000.,
                 speed=2.):
        self.base_coordinate = base_coordinate
        self.min_radius = min_radius
        self.loops_distance = loops_distance
        self.speed = speed
        self.turns = self.get_theta_from_time(time) / (2 * np.pi)
        # print(f'{self.turns = }')

    def plot(self, fig=None, ax=None, **plt_kwargs):  # отрисовать предположительную карту полета
        theta = np.linspace(0, 2 * np.pi * self.turns, int(self.turns) * 50)
        r = self.min_radius + self.loops_distance * theta
        x = r * np.cos(theta)
        y = r * np.sin(theta)
        if fig is None:
            fig, ax = plt.subplots()
        ax.plot(x, y, 'b--')
        ax.set_aspect('equal')
        return fig, ax

    def get_distance_from_angle(self, theta):
        return 0.5 * self.loops_distance * (theta * np.sqrt(1 + theta * theta) + np.arcsinh(theta))

    def get_distance_from_time(self, t):
        return self.speed * t

    def get_theta_from_time(self, t):
        return \
            fsolve(lambda theta: self.get_distance_from_angle(theta) -
                   self.get_distance_from_time(t), np.array([0.]))[0]

    def get_coordinate_from_time(self, t):
        theta = self.get_theta_from_time(t)
        r = self.min_radius + self.loops_distance * theta
        x = r * np.cos(theta)
        y = r * np.sin(theta)
        return np.array([x, y, self.base_coordinate[2]])

    def get_velocity_from_time(self, t):
        dt = 0.01
        return (self.get_coordinate_from_time(t + dt) - self.get_coordinate_from_time(t)) / dt

    def get_heading_from_time(self, t):
        velocity = self.get_velocity_from_time(t)
        # print(f'{velocity = }')
        return np.arctan2(velocity[0], velocity[1])


class Plane:
    """
    Класс самолета. В собсвенной системе координат нос самолета направлен по x, левое крыло по y
    """

    def __init__(self, base_coordinate=np.array([0., 0., 100.]), initial_time=0., initial_heading=0., initial_pitch=0.,
                 initial_roll=0., camera_direction=np.array([0., 0., -1]), speed=2.,
                 expected_time=1000., loops_distance=5.,
                 area=None):
        """
        :param base_coordinate: Координата точки начала
        :param initial_time: Начальное время
        :param initial_heading: Начальный курс, по умолчанию - север
        :param initial_pitch: начальный тангаж, по умолчанию - ровно
        :param initial_roll: начальный крен, по умолчанию - ровно
        :param camera_direction: направление камеры, по умолчанию - вниз
        :param speed: начальная скорость
        :param expected_time: ожидаемое время полета
        :param loops_distance: расстояние между петлями траектории
        :param area: размер карты
        """

        if area is None:
            self.area = [-250., 250, -250, 250]
        else:
            self.area = copy(area)
        self.time = initial_time
        self.base_coordinate = self.coordinate = base_coordinate  # в метрах
        self.Rot = R.from_matrix(np.diag([1., 1., 1.]))
        # self.heading=initial_heading
        # self.pitch=initial_pitch
        # self.roll=initial_roll
        self.set_rotation_matrix(initial_heading, initial_pitch, initial_roll)
        self.speed = speed  # в м/с
        self.expected_time = expected_time
        self.camera = Camera(camera_direction=camera_direction)
        self.map = Map(area=self.area)
        self.trajectory = Trajectory(
            speed=self.speed, time=self.expected_time, loops_distance=loops_distance)

    def __repr__(self):
        return "===================================================================================\n" \
               "Текущая локальная координата: {:.2f} {:.2f}\nВысота: {:.2f}\nКурс на север: {:.2f} градусов" \
               "\nТангаж: {:.2f} градусов\nКрен: {:.2f} градусов".format(self.coordinate[0], self.coordinate[1],
                                                                         self.coordinate[2], self.get_heading_deg(
               ),
                   self.get_pitch_deg(), self.get_roll_deg())

    def set_rotation_matrix(self, heading, pitch, roll):
        self.Rot = R.from_matrix(np.diag([1., 1., 1.]))
        self.Rot = R.from_euler('z',
                                -heading + np.pi / 2) * self.Rot  # т.к. направление мы меняем в глобальной системе координат, а ном самолета изначально по оси x
        # т.к. направление мы меняем в локальной системе координат
        self.Rot = self.Rot * R.from_euler('y', -pitch)
        # т.к. направление мы меняем в локальной системе координат
        self.Rot = self.Rot * R.from_euler('x', roll)

    @property
    def heading(self):  # курс к северу в радианах, восток  положительный
        nose_vec = self.get_nose_orientation()
        return np.arctan2(nose_vec[0], nose_vec[1])  # к северу, то есть y

    @heading.setter
    def heading(self, angle):  # углол в радианах
        self.set_rotation_matrix(angle, self.pitch, self.roll)

    def add_heading(self, angle):  # углол в радианах
        self.heading += angle

    def get_heading_deg(self):  # курс к северу в градусах, восток  положительный
        return self.heading * 180 / np.pi

    @property
    def pitch(self):  # курс к северу в радианах, восток  положительный
        nose_vec = self.get_nose_orientation()
        return np.arcsin(nose_vec[2])  # неправильно, исправить

    @pitch.setter
    def pitch(self, angle):  # углол в радианах
        self.set_rotation_matrix(self.heading, angle, self.roll)

    def add_pitch(self, angle):  # углол в радианах
        self.pitch += angle

    def get_pitch_deg(self):  # курс к северу в градусах, восток  положительный
        return self.pitch * 180 / np.pi

    @property
    def roll(self):  # курс к северу в радианах, восток  положительный
        wing_vec = self.get_wing_orientation()
        return -np.arcsin(wing_vec[2])  # неправильно, исправить

    @roll.setter
    def roll(self, angle):  # углол в радианах
        self.set_rotation_matrix(self.heading, self.pitch, angle)

    def add_roll(self, angle):  # углол в радианах
        self.roll += angle

    def get_roll_deg(self):  # курс к северу в градусах, восток  положительный
        return self.roll * 180 / np.pi

    def update_coordinate_and_time(self, t):
        self.coordinate = self.trajectory.get_coordinate_from_time(t)
        self.heading = self.trajectory.get_heading_from_time(t)
        self.time = t

    def get_nose_orientation(self):
        return self.Rot.apply([1, 0, 0])
        # return np.array([np.sin(self.phi) * np.cos(self.theta), np.cos(self.phi) * np.cos(self.theta),
        #                  np.sin(self.theta)])  # курс считается от y

    def get_velocity_vector(self):
        return self.speed * self.get_nose_orientation()

    def get_wing_orientation(self):  # от левого крыла к правому
        return self.Rot.apply([0, -1, 0])
        # return np.array([np.cos(self.phi) * np.cos(self.roll), np.sin(self.phi) * np.cos(self.roll),
        #                 -np.sin(self.roll)])

    def get_camera_orientation(self):
        return self.Rot.apply(self.camera.camera_direction)
        # return np.cross(self.get_nose_orientation(), self.get_wing_orientation())

    def get_new_temperature_points(self):
        direction_array = self.Rot.apply(self.camera.get_sensors_array())
        print(f'{direction_array = }')
        direction_array = direction_array[:, :2] / \
            direction_array[:, 2][..., np.newaxis]
        new_points = direction_array[:, :2] * \
            self.coordinate[2] + self.coordinate[:2]
        # print(f'{new_points.shape = }')
        return new_points, self.camera.get_temperature_array(new_points), self.time * np.ones([new_points.shape[0], 1])

    def add_new_temperature_points(self):
        self.map.add_points(*self.get_new_temperature_points())

    def add_new_flight_point(self):
        self.map.add_flight_point(self.coordinate)

    def show_plane(self, fig=None, ax=None):
        if fig is None:
            fig, ax = plt.subplots()
        plane_ico = 10 * \
            np.array([[0., 0., 0., ], [-1., 1., 0.], [2.5, 0, 0], [-1, -1, 0]])
        plane_ico = self.Rot.apply(plane_ico)
        plane_ico += self.coordinate
        plane_polygon = Polygon(plane_ico[:, :2], color='r')
        ax.add_patch(plane_polygon)
        return fig, ax

    def plot(self, fig=None, ax=None, show_background=True, show_grid_temperature=True, show_temperature_points=False, show_path=True,
             show_trajectory=True, show_plane=True):
        if fig is None:
            fig, ax = plt.subplots()
        if show_background:
            fig, ax = self.map.show_background(fig, ax)

        if show_grid_temperature:
            fig, ax = self.map.show_regular_grid(fig, ax)

        if show_temperature_points:
            fig, ax = self.map.show_data_points(fig, ax)

        if show_path:
            fig, ax = self.map.show_path(fig, ax)

        if show_trajectory:
            fig, ax = self.trajectory.plot(fig, ax)

        if show_plane:
            fig, ax = self.show_plane(fig, ax)

        plt.show()


flight_time = 15 * 60
area = [-400., 400, -400, 400]

plane = Plane(speed=6., initial_roll=0.01 * np.pi,
              expected_time=flight_time, area=area, loops_distance=10)

print(plane)
i = 0
for t in np.linspace(0, flight_time * 0.8, 200):
    if i % 4:
        plane.add_new_temperature_points()
        plane.add_new_flight_point()
    i += 1
    plane.update_coordinate_and_time(t)

print(plane)

plane.plot(show_grid_temperature=True, show_temperature_points=False)
