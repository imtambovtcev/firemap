import math
import os
from copy import copy

from .geometry import (Matrix, Vector, concatenate_to_list_of_vectors,
                       linspace, meshgrid, newton_raphson_method)


class Camera:
    """
    Класс, представляющий камеру.

    Attributes:
        camera_direction (Vector): Вектор направления камеры.
        sensors_array (list): Массив сенсоров камеры.
    """

    def __init__(self, camera_direction=Vector([0., 0., -1])):
        """
        Инициализация объекта Camera.

        Args:
            camera_direction (Vector, optional): Вектор направления камеры. По умолчанию [0., 0., -1].
        """
        self.camera_direction = Vector(camera_direction)
        x = linspace(-0.5, 0.5, 8)
        y = linspace(-0.5, 0.5, 8)

        calibration_grid = []
        for y_val in y:
            for x_val in x:
                norm = (x_val**2 + y_val**2 + 1)**0.5
                normalized_point = Vector([x_val / norm, y_val / norm])
                calibration_grid.append(normalized_point)

        arcsin_calibration_grid = []
        for vector in calibration_grid:
            arcsin_vector = Vector([math.asin(component)
                                   for component in vector])
            arcsin_calibration_grid.append(arcsin_vector)

        self.sensors_array = [Matrix.from_euler('yx', list(xy)).apply(self.camera_direction)
                              for xy in arcsin_calibration_grid]

    def get_camera_direction(self):
        """
        Возвращает вектор направления камеры.

        Returns:
            Vector: Вектор направления камеры.
        """
        return self.camera_direction

    def get_sensors_array(self):
        """
        Возвращает массив сенсоров камеры.

        Returns:
            list: Массив сенсоров камеры.
        """
        return self.sensors_array

    def get_temperature_array(self, points):
        """
        Возвращает массив температур для заданных точек.

        Args:
            points (list): Список точек.

        Returns:
            list: Массив температур.
        """
        t_array = [math.exp(-0.002 * ((vector[0]-100)**2 +
                            (vector[0]-120)**2)) * 50 + 17 + .3 for vector in points]
        return t_array


class Map:
    """
    Класс, представляющий карту.

    Attributes:
        area (list): Размеры области карты.
        points (list): Список точек на карте.
        temps (list): Список температур на карте.
        times (list): Список временных меток.
        flight_history (list): История полетов.
    """

    def __init__(self, area=[-250, 250, -250, 250]):
        """
        Инициализация объекта Map.

        Args:
            area (list, optional): Размеры области карты. По умолчанию [-250, 250, -250, 250].
        """
        self.points = []
        self.temps = []
        self.times = []
        self.flight_history = []
        self.area = area

    def make_regular_grid(self):
        """
        Создает регулярную сетку на карте.

        Returns:
            tuple: Координаты x и y, и результат интерполяции.
        """
        xn = yn = 100
        x = linspace(self.area[0], self.area[1], xn)
        y = linspace(self.area[2], self.area[3], yn)

        grid_x, grid_y = meshgrid(x, y)

        grid_vectors = []
        for i in range(xn):
            for j in range(yn):
                grid_vectors.append(Vector([grid_x[i][j], grid_y[i][j]]))

        interpolated_values = [0] * len(grid_vectors)

        result = []
        for i in range(xn):
            result.append(interpolated_values[i * yn: (i + 1) * yn])

        return x, y, result

    def add_points(self, new_points, new_temps, new_times):
        """
        Добавляет точки, температуры и временные метки на карту.

        Args:
            new_points (list): Список новых точек.
            new_temps (list): Список новых температур.
            new_times (list): Список новых временных меток.
        """
        self.points = concatenate_to_list_of_vectors((self.points, new_points))
        self.temps = list(self.temps) + list(new_temps)
        self.times = list(self.times) + list(new_times)

    def add_flight_point(self, new_flight_point):
        """
        Добавляет точку полета на карту.

        Args:
            new_flight_point (Vector): Новая точка полета.
        """
        self.flight_history = concatenate_to_list_of_vectors(
            [self.flight_history, [new_flight_point]])


class Trajectory:
    """
    Класс, представляющий траекторию полета.

    Attributes:
        base_coordinate (Vector): Базовая координата.
        min_radius (float): Минимальный радиус.
        loops_distance (float): Расстояние между петлями.
        time (float): Время полета.
        speed (float): Скорость полета.
        turns (float): Количество оборотов.
    """

    def __init__(self, base_coordinate=Vector([0., 0., 100.]), min_radius=0., loops_distance=5., time=2000., speed=2.):
        """
        Инициализация объекта Trajectory.

        Args:
            base_coordinate (Vector, optional): Базовая координата. По умолчанию [0., 0., 100.].
            min_radius (float, optional): Минимальный радиус. По умолчанию 0..
            loops_distance (float, optional): Расстояние между петлями. По умолчанию 5..
            time (float, optional): Время полета. По умолчанию 2000..
            speed (float, optional): Скорость полета. По умолчанию 2..
        """
        self.base_coordinate = base_coordinate
        self.min_radius = min_radius
        self.loops_distance = loops_distance
        self.speed = speed
        self.turns = self.get_theta_from_time(time) / (2 * math.pi)

    def get_distance_from_angle(self, theta):
        """
        Возвращает расстояние от угла.

        Args:
            theta (float): Угол.

        Returns:
            float: Расстояние от угла.
        """
        return 0.5 * self.loops_distance * (theta * math.sqrt(1 + theta * theta) + math.asinh(theta))

    def get_distance_from_time(self, t):
        """
        Возвращает расстояние от времени.

        Args:
            t (float): Время.

        Returns:
            float: Расстояние от времени.
        """
        return self.speed * t

    def get_theta_from_time(self, t):
        """
        Возвращает угол от времени.

        Args:
            t (float): Время.

        Returns:
            float: Угол от времени.
        """
        def f(theta):
            return 0.5 * self.loops_distance * (theta * math.sqrt(1 + theta**2) + math.asinh(theta)) - self.speed * t

        def df(theta):
            return 0.5 * self.loops_distance * (math.sqrt(1 + theta**2) + (theta**2) / math.sqrt(1 + theta**2) + 1 / math.sqrt(1 + theta**2))

        initial_guess = 0.0

        return newton_raphson_method(f, df, initial_guess)

    def get_coordinate_from_time(self, t):
        """
        Возвращает координату от времени.

        Args:
            t (float): Время.

        Returns:
            Vector: Координата от времени.
        """
        theta = self.get_theta_from_time(t)
        r = self.min_radius + self.loops_distance * theta
        x = r * math.cos(theta)
        y = r * math.sin(theta)
        return Vector([x, y, self.base_coordinate[2]])

    def get_velocity_from_time(self, t):
        """
        Возвращает скорость от времени.

        Args:
            t (float): Время.

        Returns:
            Vector: Скорость от времени.
        """
        dt = 0.01
        return (self.get_coordinate_from_time(t + dt) - self.get_coordinate_from_time(t)) / dt

    def get_heading_from_time(self, t):
        """
        Возвращает направление от времени.

        Args:
            t (float): Время.

        Returns:
            float: Направление от времени.
        """
        velocity = self.get_velocity_from_time(t)
        return math.atan2(velocity[0], velocity[1])


class Plane:
    """
    Класс, представляющий самолет.

    Attributes:
        base_coordinate (Vector): Базовая координата.
        initial_time (float): Начальное время.
        initial_heading (float): Начальный курс.
        initial_pitch (float): Начальный тангаж.
        initial_roll (float): Начальный крен.
        camera_direction (Vector): Направление камеры.
        speed (float): Скорость полета.
        expected_time (float): Ожидаемое время полета.
        loops_distance (float): Расстояние между петлями траектории.
        area (list): Размеры карты.
    """

    def __init__(self, base_coordinate=Vector([0., 0., 100.]), initial_time=0., initial_heading=0., initial_pitch=0.,
                 initial_roll=0., camera_direction=Vector([0., 0., -1]), speed=2.,
                 expected_time=1000., loops_distance=5.,
                 area=None):
        """
        Инициализация объекта Plane.

        Args:
            base_coordinate (Vector, optional): Базовая координата. По умолчанию [0., 0., 100.].
            initial_time (float, optional): Начальное время. По умолчанию 0..
            initial_heading (float, optional): Начальный курс. По умолчанию 0..
            initial_pitch (float, optional): Начальный тангаж. По умолчанию 0..
            initial_roll (float, optional): Начальный крен. По умолчанию 0..
            camera_direction (Vector, optional): Направление камеры. По умолчанию [0., 0., -1].
            speed (float, optional): Скорость полета. По умолчанию 2..
            expected_time (float, optional): Ожидаемое время полета. По умолчанию 1000..
            loops_distance (float, optional): Расстояние между петлями траектории. По умолчанию 5..
            area (list, optional): Размеры карты. По умолчанию None.
        """
        if area is None:
            self.area = [-250., 250, -250, 250]
        else:
            self.area = copy(area)
        self.time = initial_time
        self.base_coordinate = self.coordinate = base_coordinate
        self.Rot = Matrix.from_matrix(Matrix.diag([1., 1., 1.]))
        self.set_rotation_matrix(initial_heading, initial_pitch, initial_roll)
        self.speed = speed
        self.expected_time = expected_time
        self.camera = Camera(camera_direction=camera_direction)
        self.map = Map(area=self.area)
        self.trajectory = Trajectory(
            speed=self.speed, time=self.expected_time, loops_distance=loops_distance)

    def set_rotation_matrix(self, heading, pitch, roll):
        """
        Устанавливает матрицу поворота.

        Args:
            heading (float): Курс.
            pitch (float): Тангаж.
            roll (float): Крен.
        """
        self.Rot = Matrix.from_matrix(Matrix.diag([1., 1., 1.]))
        self.Rot = Matrix.from_euler('z', [-heading + math.pi / 2]) * self.Rot
        self.Rot = self.Rot * Matrix.from_euler('y', [-pitch])
        self.Rot = self.Rot * Matrix.from_euler('x', [roll])

    @property
    def heading(self):
        """
        Курс к северу в радианах.

        Returns:
            float: Курс к северу в радианах.
        """
        nose_vec = self.get_nose_orientation()
        return math.atan2(nose_vec[0], nose_vec[1])

    @heading.setter
    def heading(self, angle):
        """
        Устанавливает курс к северу в радианах.

        Args:
            angle (float): Угол в радианах.
        """
        self.set_rotation_matrix(angle, self.pitch, self.roll)

    def add_heading(self, angle):
        """
        Добавляет угол к курсу к северу в радианах.

        Args:
            angle (float): Угол в радианах.
        """
        self.heading += angle

    def get_heading_deg(self):
        """
        Возвращает курс к северу в градусах.

        Returns:
            float: Курс к северу в градусах.
        """
        return self.heading * 180 / math.pi

    @property
    def pitch(self):
        """
        Тангаж в радианах.

        Returns:
            float: Тангаж в радианах.
        """
        nose_vec = self.get_nose_orientation()
        return math.atan2(nose_vec[2], math.sqrt(nose_vec[0]**2 + nose_vec[1]**2))

    @property
    def roll(self):
        """
        Крен в радианах.

        Returns:
            float: Крен в радианах.
        """
        nose_vec = self.get_nose_orientation()
        return math.atan2(nose_vec[1], nose_vec[0])

    def get_nose_orientation(self):
        """
        Возвращает ориентацию носа самолета.

        Returns:
            Vector: Ориентация носа самолета.
        """
        return self.Rot.apply(Vector([1., 0., 0.]))

    def get_pitch_deg(self):
        """
        Возвращает тангаж в градусах.

        Returns:
            float: Тангаж в градусах.
        """
        return self.pitch * 180 / math.pi

    def get_roll_deg(self):
        """
        Возвращает крен в градусах.

        Returns:
            float: Крен в градусах.
        """
        return self.roll * 180 / math.pi

    def __repr__(self):
        """
        Возвращает строковое представление объекта Plane.

        Returns:
            str: Строковое представление объекта Plane.
        """
        return "===================================================================================\n" \
               "Текущая локальная координата: {:.2f} {:.2f}\nВысота: {:.2f}\nКурс на север: {:.2f} градусов" \
               "\nТангаж: {:.2f} градусов\nКрен: {:.2f} градусов".format(self.coordinate[0], self.coordinate[1],
                                                                         self.coordinate[2], self.get_heading_deg(
               ),
                   self.get_pitch_deg(), self.get_roll_deg())
        return math.asin(nose_vec[2])  # неправильно, исправить

    @ pitch.setter
    def pitch(self, angle):  # углол в радианах
        self.set_rotation_matrix(self.heading, angle, self.roll)

    def add_pitch(self, angle):  # углол в радианах
        self.pitch += angle

    def get_pitch_deg(self):  # курс к северу в градусах, восток  положительный
        return self.pitch * 180 / math.pi

    @ property
    def roll(self):  # курс к северу в радианах, восток  положительный
        wing_vec = self.get_wing_orientation()
        return -math.asin(wing_vec[2])  # неправильно, исправить

    @ roll.setter
    def roll(self, angle):  # углол в радианах
        self.set_rotation_matrix(self.heading, self.pitch, angle)

    def add_roll(self, angle):  # углол в радианах
        self.roll += angle

    def get_roll_deg(self):  # курс к северу в градусах, восток  положительный
        return self.roll * 180 / math.pi

    def update_coordinate_and_time(self, t):
        self.coordinate = self.trajectory.get_coordinate_from_time(t)
        self.heading = self.trajectory.get_heading_from_time(t)
        self.time = t

    def get_nose_orientation(self):
        return self.Rot.apply(Vector([1, 0, 0]))

    def get_velocity_vector(self):
        return self.speed * self.get_nose_orientation()

    def get_wing_orientation(self):  # от левого крыла к правому
        return self.Rot.apply(Vector([0, -1, 0]))

    def get_camera_orientation(self):
        return self.Rot.apply(self.camera.camera_direction)

    def get_new_temperature_points(self):
        direction_array = self.Rot.apply(self.camera.get_sensors_array())
        print(f'{direction_array = }')
        direction_array = [Vector(vector[:2])/vector[2]
                           for vector in direction_array]
        new_points = [Vector(vector[:2])*self.coordinate[2] + Vector(self.coordinate[:2])
                      for vector in direction_array]

        return new_points, self.camera.get_temperature_array(new_points), Vector([self.time]*len(new_points))

    def add_new_temperature_points(self):
        self.map.add_points(*self.get_new_temperature_points())

    def add_new_flight_point(self):
        self.map.add_flight_point(self.coordinate)
